use std::str::FromStr;

use anyhow::{anyhow, Error};
use itertools::Itertools;

struct EnvironmentHistory(Vec<isize>, usize);

impl EnvironmentHistory {
    fn new(values: Vec<isize>) -> Self {
        Self(values, 0)
    }

    fn predict(self) -> isize {
        let mut values: Vec<_> = self.collect();
        values.reverse();

        values.iter().fold(0, |acc, el| acc + el.last().unwrap())
    }
}

impl Iterator for EnvironmentHistory {
    type Item = Vec<isize>;

    fn next(&mut self) -> Option<Self::Item> {
        self.1 += 1;

        if self.0.iter().all(|n| *n == 0) {
            None
        } else if self.1 == 1 {
            Some(self.0.clone())
        } else {
            self.0 = Vec::from_iter(self.0.iter().zip(self.0[1..].iter()).map(|(a, b)| b - a));

            Some(self.0.clone())
        }
    }
}

impl FromStr for EnvironmentHistory {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let values: Vec<isize> = s
            .split_whitespace()
            .map(|word| {
                word.parse::<isize>()
                    .map_err(|_| anyhow!("Failed to parse {}", word))
            })
            .try_collect()?;

        Ok(Self::new(values))
    }
}

pub fn run(input: &str) -> isize {
    input
        .trim()
        .split('\n')
        .map(|line| line.parse::<EnvironmentHistory>().unwrap())
        .map(|el| el.predict())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    use test_case::test_case;

    #[test]
    fn test_environment_history_iter() {
        let mut actual = EnvironmentHistory::new(vec![0, 3, 6, 9, 12, 15]);

        assert_eq!(actual.next(), Some(vec![0, 3, 6, 9, 12, 15]));
        assert_eq!(actual.next(), Some(vec![3, 3, 3, 3, 3]));
        assert_eq!(actual.next(), Some(vec![0, 0, 0, 0]));
        assert_eq!(actual.next(), None);
    }

    #[test_case(vec![0, 3, 6, 9, 12, 15], 18)]
    #[test_case(vec![1, 3, 6, 10, 15, 21], 28)]
    #[test_case(vec![10, 13, 16, 21, 30, 45], 68)]
    fn test_predict(values: Vec<isize>, expected: isize) {
        let actual = EnvironmentHistory::new(values);

        assert_eq!(actual.predict(), expected)
    }

    #[test]
    fn test_run() {
        let input = include_str!("../../data/day9_sample.txt");

        assert_eq!(run(input), 114);
    }
}

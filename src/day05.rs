// https://adventofcode.com/2023/day/5
#![allow(dead_code)]
use std::{ops::Range, str::FromStr};

use anyhow::{anyhow, Error};
use regex::Regex;

lazy_static! {
    static ref RE_MAP_DEF: Regex = Regex::new(r"^(?P<source>\w+)-to-(?P<dest>\w+) map:$").unwrap();
    static ref RE_RANGE: Regex =
        Regex::new(r"^(?P<dest>\d+)\s+(?P<source>\d+)\s+(?P<length>\d+)$").unwrap();
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
enum MapSymbol {
    Seed,
    Soil,
    Fertilizer,
    Water,
    Light,
    Temperature,
    Humidity,
    Location,
}

impl FromStr for MapSymbol {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "seed" => Ok(Self::Seed),
            "soil" => Ok(Self::Soil),
            "fertilizer" => Ok(Self::Fertilizer),
            "water" => Ok(Self::Water),
            "light" => Ok(Self::Light),
            "temperature" => Ok(Self::Temperature),
            "humidity" => Ok(Self::Humidity),
            "location" => Ok(Self::Location),
            _ => Err(anyhow!("Unknown map symbol: {}", s)),
        }
    }
}

#[derive(Debug, Default, PartialEq, Eq)]
struct MapRanges(Range<u64>, Range<u64>);

impl FromStr for MapRanges {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let captures = RE_RANGE
            .captures(s)
            .ok_or(anyhow!("Failed to parse Ranges from {}", s))?;

        let source_start: u64 = captures
            .name("source")
            .ok_or(anyhow!("Failed to parse Ranges.source"))?
            .as_str()
            .parse()?;

        let dest_start: u64 = captures
            .name("dest")
            .ok_or(anyhow!("Failed to parse Ranges.source"))?
            .as_str()
            .parse()?;

        let range_length: u64 = captures
            .name("length")
            .ok_or(anyhow!("Failed to parse Ranges.source"))?
            .as_str()
            .parse()?;

        Ok(Self(
            source_start..source_start + range_length,
            dest_start..dest_start + range_length,
        ))
    }
}

#[derive(Debug)]
struct Seeds(Box<[u64]>);

impl FromStr for Seeds {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(
            s.split(' ').filter_map(|word| word.parse().ok()).collect(),
        ))
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Map {
    source: MapSymbol,
    destination: MapSymbol,
    ranges: Box<[MapRanges]>,
}

impl Map {
    fn map(&self, source: u64) -> u64 {
        self.ranges
            .iter()
            .find_map(|range| {
                if let Some(i) = range.0.clone().position(|n| n == source) {
                    range.1.clone().nth(i)
                } else {
                    None
                }
            })
            .unwrap_or(source)
    }
}

impl FromStr for Map {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut iter = s.split('\n');

        let captures = RE_MAP_DEF
            .captures(iter.next().ok_or(anyhow!("No data recieved"))?)
            .ok_or(anyhow!("Failed to parse Map definition: {}", s))?;

        let source: MapSymbol = captures
            .name("source")
            .ok_or(anyhow!("Failed to parse Map.source"))?
            .as_str()
            .parse()?;

        let destination: MapSymbol = captures
            .name("dest")
            .ok_or(anyhow!("Failed to parse Map.destination"))?
            .as_str()
            .parse()?;

        let ranges: Box<[MapRanges]> = iter
            .by_ref()
            .map_while(|line| {
                if RE_RANGE.is_match(line) {
                    line.parse().ok()
                } else {
                    None
                }
            })
            .collect();

        if let Some(line) = iter.next() {
            return Err(anyhow!("Leftover data: {}", line));
        }

        Ok(Self {
            source,
            destination,
            ranges,
        })
    }
}

struct FullMap(Box<[Map]>);

impl FullMap {
    fn map(&self, seed: u64, src: MapSymbol, dest: MapSymbol) -> u64 {
        self.0
            .iter()
            .skip_while(|map| map.source != src)
            .take_while(|map| map.source != dest)
            .fold((seed, src), |acc, el| {
                if el.source != acc.1 {
                    acc
                } else {
                    (el.map(acc.0), el.destination)
                }
            })
            .0
    }
}

impl FromIterator<Map> for FullMap {
    fn from_iter<T: IntoIterator<Item = Map>>(iter: T) -> Self {
        let mut sorted = Vec::from_iter(iter);
        sorted.sort_by(|a, b| a.source.cmp(&b.source));

        Self(Box::from_iter(sorted))
    }
}

pub fn run(input: &[&str]) -> u64 {
    let mut iter = input.iter();

    let seeds: Seeds = iter
        .next()
        .ok_or(anyhow!("No data recieved"))
        .unwrap()
        .parse()
        .unwrap();

    let map: FullMap = iter.map(|chunk| chunk.parse().unwrap()).collect();

    seeds
        .0
        .iter()
        .map(|seed| map.map(*seed, MapSymbol::Seed, MapSymbol::Location))
        .min()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    use test_case::test_case;
    use textwrap::dedent;

    #[test]
    fn test_map_from_str() {
        let input = dedent(
            "
            seed-to-soil map:
            50 98 2
            52 50 48
        ",
        );

        let expected = Map {
            source: MapSymbol::Seed,
            destination: MapSymbol::Soil,
            ranges: Box::from([MapRanges(98..100, 50..52), MapRanges(50..98, 52..100)]),
        };
        let actual = input.trim().parse().unwrap();

        assert_eq!(expected, actual)
    }

    #[test_case(1, 1)]
    #[test_case(51, 53)]
    #[test_case(99, 51)]
    fn test_map_map(seed: u64, expected: u64) {
        let map = Map {
            source: MapSymbol::Seed,
            destination: MapSymbol::Soil,
            ranges: Box::from([MapRanges(98..100, 50..52), MapRanges(50..98, 52..100)]),
        };

        assert_eq!(map.map(seed), expected)
    }

    #[test]
    fn test_run() {
        let input: Box<[&str]> = include_str!("../data/day5_sample.txt")
            .trim()
            .split("\n\n")
            .collect();

        assert_eq!(run(&input), 35);
    }
}

use std::{collections::HashMap, str::FromStr};

use anyhow::{anyhow, Error, Ok};
use itertools::Itertools;
use regex::Regex;

lazy_static! {
    static ref RE_OP: Regex = Regex::new(r"^(?P<label>\w+)(?P<op>[-=])(?P<len>\d)?$").unwrap();
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Hash(u32);

impl FromStr for Hash {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let hash = s
            .chars()
            .map(u32::from)
            .fold(0u32, |acc, ch| ((acc + ch) * 17) % 256);

        Ok(Self(hash))
    }
}

impl From<Hash> for u32 {
    fn from(value: Hash) -> Self {
        value.0
    }
}

impl std::iter::Sum for Hash {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        Self(iter.fold(0, |acc, el| acc + el.0))
    }
}

#[derive(Debug, Clone)]
struct Lens {
    label: String,
    focal_length: Option<u8>,
}

impl Lens {
    fn new<T>(label: T, focal_length: Option<u8>) -> Self
    where
        T: ToString,
    {
        Self {
            label: label.to_string(),
            focal_length,
        }
    }
}

impl PartialEq for Lens {
    fn eq(&self, other: &Self) -> bool {
        self.label == other.label
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
struct LensBox(Vec<Lens>);

impl LensBox {
    fn op(self, op: Operation) -> Self {
        let updated = match op {
            Operation::Append(new_lens) => match self.0.iter().contains(&new_lens) {
                true => self
                    .0
                    .into_iter()
                    .map(|lens| match lens == new_lens {
                        true => new_lens.clone(),
                        false => lens,
                    })
                    .collect(),
                false => [self.0, vec![new_lens]].concat(),
            },
            Operation::Remove(new_lens) => self
                .0
                .into_iter()
                .filter(|lens| lens != &new_lens)
                .collect(),
        };

        Self(updated)
    }
}

#[derive(Debug, Default)]
struct FocusSequence(HashMap<u32, LensBox>);

impl FocusSequence {
    fn focusing_power(&self) -> usize {
        self.0.iter().fold(0, |acc, (&box_n, lens_box)| {
            acc + lens_box
                .0
                .clone()
                .into_iter()
                .enumerate()
                .fold(0, |acc0, (slot_n, lens)| {
                    acc0 + [
                        box_n as usize + 1,
                        slot_n + 1,
                        lens.focal_length.unwrap() as usize,
                    ]
                    .into_iter()
                    .product::<usize>()
                })
        })
    }
}

#[derive(Debug, PartialEq)]
enum Operation {
    Remove(Lens),
    Append(Lens),
}

impl FromStr for Operation {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let captures = RE_OP
            .captures(s)
            .ok_or(anyhow!("Failed to parse operation from {}", s))?;

        let label = captures
            .name("label")
            .ok_or(anyhow!("Failed to parse label from {}", s))?
            .as_str();

        let op = captures
            .name("op")
            .ok_or(anyhow!("Failed to parse op from {}", s))?
            .as_str();

        let focal_length: Option<u8> = captures.name("len").and_then(|m| m.as_str().parse().ok());

        let lens = Lens::new(label, focal_length);

        match op {
            "-" => Ok(Self::Remove(lens)),
            "=" if focal_length.is_some() => Ok(Self::Append(lens)),
            "=" => Err(anyhow!("Got None focal length for Append operation")),
            _ => unreachable!(),
        }
    }
}

pub fn run(input: &str) -> usize {
    let mut sequence = FocusSequence::default();

    input.trim().split(',').for_each(|op| {
        let parsed_op: Operation = op.parse().unwrap();

        let k: u32 = match parsed_op {
            Operation::Append(lens) => lens.label.parse::<Hash>().unwrap().into(),
            Operation::Remove(lens) => lens.label.parse::<Hash>().unwrap().into(),
        };

        let old_box = match sequence.0.get(&k) {
            Some(val) => val.clone(),
            None => LensBox::default(),
        };

        sequence.0.insert(k, old_box.op(op.parse().unwrap()));
    });

    sequence.focusing_power()
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    #[test]
    fn test_hash_from_str() {
        assert_eq!("HASH".parse::<Hash>().unwrap(), Hash(52))
    }

    #[test_case("rn=1", Operation::Append(Lens::new("rn", Some(1))))]
    #[test_case("qp-", Operation::Remove(Lens::new("qp", None)))]
    fn test_op_from_str(input: &str, expected: Operation) {
        assert_eq!(input.parse::<Operation>().unwrap(), expected)
    }

    #[test]
    fn test_lens_box_op() {
        let mut actual = LensBox::default();

        actual = actual.op("rn=1".parse().unwrap());
        assert_eq!(actual, LensBox(vec![Lens::new("rn", Some(1))]));

        actual = actual.op("cm=2".parse().unwrap());
        assert_eq!(
            actual,
            LensBox(vec![Lens::new("rn", Some(1)), Lens::new("cm", Some(2))])
        );

        actual = actual.op("rn=2".parse().unwrap());
        assert_eq!(
            actual,
            LensBox(vec![Lens::new("rn", Some(2)), Lens::new("cm", Some(2))])
        );

        actual = actual.op("cm-".parse().unwrap());
        assert_eq!(actual, LensBox(vec![Lens::new("rn", Some(2))]));
    }

    #[test]
    fn test_run() {
        let input = "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";

        assert_eq!(run(input), 145)
    }
}

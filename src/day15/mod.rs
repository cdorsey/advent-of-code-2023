// https://adventofcode.com/2023/day/15
mod part1;
mod part2;

pub use self::part1::run as part1;
pub use self::part2::run as part2;

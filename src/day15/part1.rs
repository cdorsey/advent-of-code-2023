use std::str::FromStr;

use anyhow::Error;

#[derive(Debug, PartialEq, Eq)]
struct Hash(u32);

impl FromStr for Hash {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let hash = s
            .chars()
            .map(u32::from)
            .fold(0u32, |acc, ch| ((acc + ch) * 17) % 256);

        Ok(Self(hash))
    }
}

impl From<Hash> for u32 {
    fn from(value: Hash) -> Self {
        value.0
    }
}

impl std::iter::Sum for Hash {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        Self(iter.fold(0, |acc, el| acc + el.0))
    }
}

pub fn run(input: &str) -> u32 {
    input
        .trim()
        .split(',')
        .map(|seq| seq.parse().unwrap())
        .sum::<Hash>()
        .into()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash_from_str() {
        assert_eq!("HASH".parse::<Hash>().unwrap(), Hash(52))
    }

    #[test]
    fn test_run() {
        let input = "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";

        assert_eq!(run(input), 1320)
    }
}

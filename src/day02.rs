// https://adventofcode.com/2023/day/2
#![allow(dead_code)]

use std::{cmp::Ordering, str::FromStr};

use anyhow::{anyhow, Error, Result};
use regex::Regex;

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Game {
    id: usize,
    cube_sets: Box<[CubeSet]>,
}

impl Game {
    pub fn min_dice(&self) -> CubeSet {
        self.cube_sets
            .iter()
            .fold(CubeSet::default(), |acc, el| CubeSet {
                red: acc.red.max(el.red),
                green: acc.green.max(el.green),
                blue: acc.blue.max(el.blue),
            })
    }
}

impl FromStr for Game {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let re_game = Regex::new(r"^Game (\d+): (.*)$").unwrap();

        if let Some(captures) = re_game.captures(s) {
            Ok(Self {
                id: captures[1].parse()?,
                cube_sets: captures[2]
                    .split("; ")
                    .map(|cap| cap.parse().unwrap())
                    .collect(),
            })
        } else {
            Err(anyhow!("Failed to parse game"))
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct CubeSet {
    red: u8,
    green: u8,
    blue: u8,
}

impl CubeSet {
    pub fn red(self, n: u8) -> Self {
        Self { red: n, ..self }
    }

    pub fn green(self, n: u8) -> Self {
        Self { green: n, ..self }
    }

    pub fn blue(self, n: u8) -> Self {
        Self { blue: n, ..self }
    }

    pub fn num_dice(&self) -> u8 {
        self.red + self.blue + self.green
    }

    pub fn power(&self) -> u32 {
        u32::from(self.red) * u32::from(self.green) * u32::from(self.blue)
    }
}

impl FromStr for CubeSet {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let re_cube_set = Regex::new(r"(\d+)\s+(red|green|blue)").unwrap();

        let result: Self = re_cube_set
            .find_iter(s)
            .map(|color| color.as_str().split(' ').collect::<Box<[&str]>>())
            .map(|exploded| (exploded[0].parse::<u8>().unwrap(), exploded[1]))
            .fold(Default::default(), |acc, el| match el {
                (n, "red") => acc.red(n),
                (n, "green") => acc.green(n),
                (n, "blue") => acc.blue(n),
                (_, color) => panic!("Unexpected color: {}", color),
            });

        Ok(result)
    }
}

impl PartialOrd for CubeSet {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        if self.red == other.red
            && self.green == other.green
            && self.blue == other.blue
            && self.num_dice() == other.num_dice()
        {
            Some(Ordering::Equal)
        } else if self.red > other.red
            || self.green > other.green
            || self.blue > other.blue
            || self.num_dice() > other.num_dice()
        {
            Some(Ordering::Greater)
        } else {
            Some(Ordering::Less)
        }
    }
}

pub fn run(input: &[&str], _all_cubes: CubeSet) -> u32 {
    input
        .iter()
        .map(|line| line.parse().unwrap())
        .map(|game: Game| game.min_dice().power())
        .sum()
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    #[test_case("3 blue, 4 red", CubeSet { red: 4, blue: 3, green: 0 })]
    #[test_case("1 red, 2 green, 6 blue", CubeSet { red: 1, green: 2, blue: 6})]
    #[test_case("2 green", CubeSet { red: 0, green: 2, blue: 0})]
    fn test_cube_set_from_str(input: &str, expected: CubeSet) {
        let actual: CubeSet = input.parse().unwrap();

        assert!(actual == expected)
    }

    #[test]
    fn test_game_from_str() {
        let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green";
        let expected = Game {
            id: 1,
            cube_sets: Box::new([
                CubeSet {
                    red: 4,
                    blue: 3,
                    green: 0,
                },
                CubeSet {
                    red: 1,
                    blue: 6,
                    green: 2,
                },
                CubeSet {
                    red: 0,
                    green: 2,
                    blue: 0,
                },
            ]),
        };

        let actual: Game = input.parse().unwrap();

        assert!(actual == expected)
    }

    #[test_case("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green", 48)]
    #[test_case("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", 12)]
    #[test_case(
        "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
        1560
    )]
    #[test_case(
        "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
        630
    )]
    #[test_case("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green", 36)]
    fn test_find_min_cubes(game_str: &str, expected: u32) {
        let game: Game = game_str.parse().unwrap();

        assert!(game.min_dice().power() == expected)
    }
}

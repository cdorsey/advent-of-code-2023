// https://adventofcode.com/2023/day/6

#[derive(Debug)]
struct Race {
    race_time: u64,
    distance_to_beat: u64,
}

impl Race {
    fn new(race_time: u64, distance_to_beat: u64) -> Self {
        Self {
            race_time,
            distance_to_beat,
        }
    }

    fn num_winning_times(&self) -> u64 {
        (0..=self.race_time)
            .zip((0..=self.race_time).rev())
            .map(|(button, travel)| button * travel)
            .filter(|dist_traveled| *dist_traveled > self.distance_to_beat)
            .count()
            .try_into()
            .unwrap()
    }
}

pub fn run(input: &str) -> u64 {
    let mut iter = input.split('\n').map(|line| {
        line.split(": ")
            .nth(1)
            .unwrap()
            .replace(' ', "")
            .parse::<u64>()
            .unwrap()
    });

    let time = iter.next().unwrap();

    let distance = iter.next().unwrap();

    Race::new(time, distance).num_winning_times()
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    #[test_case(7, 9, 4)]
    #[test_case(15, 40, 8)]
    #[test_case(30, 200, 9)]
    fn test_num_winning_times(race_time: u64, distance_to_beat: u64, expected: u64) {
        assert_eq!(
            Race::new(race_time, distance_to_beat).num_winning_times(),
            expected
        );
    }

    #[test]
    fn test_run() {
        let input = include_str!("../data/day6_sample.txt");

        assert_eq!(run(input), 71503);
    }
}

use std::collections::BTreeMap;
use std::fmt::Display;
use std::str::FromStr;

use anyhow::{anyhow, Error};
use itertools::FoldWhile::{Continue, Done};
use itertools::Itertools;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Tile {
    Ash,
    Rock,
}

impl From<char> for Tile {
    fn from(value: char) -> Self {
        match value {
            '#' => Self::Rock,
            '.' => Self::Ash,
            _ => unreachable!(),
        }
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Ash => write!(f, "Ash "),
            Self::Rock => write!(f, "Rock"),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Map {
    data: Vec<Tile>,
    height: usize,
    width: usize,
}

impl Map {
    fn as_cols(&self) -> Vec<Vec<Tile>> {
        let mut map: BTreeMap<usize, Vec<Tile>> = BTreeMap::new();

        for (i, &tile) in self.data.iter().enumerate() {
            let col_n = i % self.width;

            if let Some(tiles) = map.get(&col_n) {
                map.insert(col_n, [tiles.clone(), vec![tile]].concat());
            } else {
                map.insert(col_n, vec![tile]);
            }
        }

        map.values().cloned().collect()
    }

    fn as_rows(&self) -> Vec<Vec<Tile>> {
        self.data.chunks(self.width).map(Vec::from).collect()
    }

    fn find_midpoint(data: Vec<Vec<Tile>>) -> Option<usize> {
        data.iter()
            .enumerate()
            .tuple_windows()
            .fold_while(None, |acc, ((_, row1), (i, row2))| {
                if row1 == row2 {
                    let (left, right) = data.split_at(i);
                    let result =
                        left.iter()
                            .rev()
                            .zip(right.iter())
                            .try_fold(i, |acc, (row1, row2)| match row1 == row2 {
                                true => Some(acc),
                                false => None,
                            });

                    if result.is_some() {
                        Done(result)
                    } else {
                        Continue(acc)
                    }
                } else {
                    Continue(acc)
                }
            })
            .into_inner()
    }

    pub fn find_symmetry(&self) -> Option<usize> {
        Self::find_midpoint(self.as_cols())
            .or_else(|| Self::find_midpoint(self.as_rows()).map(|n| n * 100))
    }
}

impl FromStr for Map {
    type Err = Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        let row = value.trim().split('\n');

        let height = row.clone().collect_vec().len();
        let width = row.clone().next().ok_or(anyhow!("No column data"))?.len();

        let data: Vec<Tile> = row.flat_map(|line| line.chars().map_into()).collect();

        Ok(Self {
            data,
            width,
            height,
        })
    }
}

impl From<&str> for Map {
    fn from(value: &str) -> Self {
        value.parse().unwrap()
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in self.data.chunks(self.width) {
            for col in row {
                write!(f, "{}  ", col)?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

pub fn run(input: &str) -> usize {
    input
        .split("\n\n")
        .map_into::<Map>()
        .map(|map| {
            map.find_symmetry()
                .ok_or_else(|| anyhow!("Failed to find a symmetry\n{}", map))
        })
        .fold_ok(0, |acc, el| acc + el)
        .unwrap()
}

#[cfg(test)]
mod tests {
    use textwrap::dedent;

    use super::*;

    #[test]
    fn test_map_from_str() {
        let input = dedent(
            "
            ###
            ...
            ###
            ",
        );

        let expected = Map {
            data: vec![
                Tile::Rock,
                Tile::Rock,
                Tile::Rock,
                Tile::Ash,
                Tile::Ash,
                Tile::Ash,
                Tile::Rock,
                Tile::Rock,
                Tile::Rock,
            ],
            width: 3,
            height: 3,
        };

        assert_eq!(input.parse::<Map>().unwrap(), expected);
    }

    #[test]
    fn test_find_symmetry_col() {
        let actual: Map = dedent(
            "
            #.##..##.
            ..#.##.#.
            ##......#
            ##......#
            ..#.##.#.
            ..##..##.
            #.#.##.#.
            ",
        )
        .parse()
        .unwrap();

        assert_eq!(actual.find_symmetry(), Some(5))
    }

    #[test]
    fn test_find_symmetry_row() {
        let actual: Map = dedent(
            "
            #...##..#
            #....#..#
            ..##..###
            #####.##.
            #####.##.
            ..##..###
            #....#..#
            ",
        )
        .parse()
        .unwrap();

        assert_eq!(actual.find_symmetry(), Some(400))
    }

    #[test]
    fn test_run() {
        let input = include_str!("../../data/day13_sample.txt");

        assert_eq!(run(input), 405)
    }
}

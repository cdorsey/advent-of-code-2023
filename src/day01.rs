// https://adventofcode.com/2023/day/1
use regex::Regex;

#[allow(dead_code)]
pub fn run(lines: &[&str]) -> u32 {
    let re_first_num =
        Regex::new(r".*?(\d|one|two|three|four|five|six|seven|eight|nine).*").unwrap();
    let re_last_num =
        Regex::new(r".*(\d|one|two|three|four|five|six|seven|eight|nine).*?").unwrap();

    lines
        .iter()
        .map(|line| {
            let first_num = re_first_num.captures(line);
            let last_num = re_last_num.captures(line);

            [
                first_num.map(|c| c[1].to_string()).unwrap(),
                last_num.map(|c| c[1].to_string()).unwrap(),
            ]
        })
        .map(|exploded| {
            exploded.map(|digit| match digit.as_str() {
                "one" => "1".to_string(),
                "two" => "2".to_string(),
                "three" => "3".to_string(),
                "four" => "4".to_string(),
                "five" => "5".to_string(),
                "six" => "6".to_string(),
                "seven" => "7".to_string(),
                "eight" => "8".to_string(),
                "nine" => "9".to_string(),
                _ => digit,
            })
        })
        .map(|exploded| format!("{}{}", exploded[0], exploded[1]))
        .map(|as_str| as_str.parse::<u32>().unwrap())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample_input() {
        let sample_input = [
            "two1nine",
            "eightwothree",
            "abcone2threexyz",
            "xtwone3four",
            "4nineeightseven2",
            "zoneight234",
            "7pqrstsixteen",
        ];

        assert!(run(&sample_input) == 281)
    }
}

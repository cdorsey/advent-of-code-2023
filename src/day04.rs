// https://adventofcode.com/2023/day/4
#![allow(dead_code)]
use std::{collections::HashMap, ops::Index, str::FromStr};

use anyhow::{anyhow, Error};
use regex::Regex;

lazy_static! {
    static ref RE_CARD: Regex = Regex::new(
        r"^Card\s+(?P<id>\d+):\s+(?P<winning_numbers>(\d+\s*)+)\s+\|\s+(?P<have_numbers>(\d+\s*)+)$"
    )
    .unwrap();
}

#[derive(Debug, Default, PartialEq, Eq)]
struct Card {
    id: usize,
    winning_numbers: Box<[u8]>,
    have_numbers: Box<[u8]>,
}

impl Card {
    pub fn num_winners(&self) -> u8 {
        self.have_numbers
            .iter()
            .filter(|n| self.winning_numbers.contains(n))
            .collect::<Box<[_]>>()
            .len() as u8
    }

    #[allow(dead_code)]
    pub fn calculate_score(&self) -> u32 {
        self.have_numbers
            .iter()
            .filter(|n| self.winning_numbers.contains(n))
            .enumerate()
            .fold(
                0u32,
                |acc, (i, _)| if i <= 1 { i as u32 + 1 } else { acc * 2 },
            )
    }
}

impl FromStr for Card {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let captures = RE_CARD
            .captures(s)
            .ok_or(anyhow!("Failed to parse Card from {:?}", s))?;

        let parse_captures_to_list =
            |captures: &regex::Captures<'_>, name: &str| -> Result<Box<[u8]>, Self::Err> {
                Ok(captures
                    .name(name)
                    .ok_or(anyhow!("Failed to parse Card.{}", name))?
                    .as_str()
                    .split_whitespace()
                    .filter_map(|n| n.parse().ok())
                    .collect())
            };

        let id: usize = captures
            .name("id")
            .ok_or(anyhow!("Failed to parse Card.id"))?
            .as_str()
            .parse()?;

        let winning_numbers = parse_captures_to_list(&captures, "winning_numbers")?;

        let have_numbers = parse_captures_to_list(&captures, "have_numbers")?;

        Ok(Self {
            id,
            winning_numbers,
            have_numbers,
        })
    }
}

#[derive(Debug)]
struct CardSet(HashMap<usize, Card>);

impl Index<usize> for CardSet {
    type Output = Card;

    fn index(&self, index: usize) -> &Self::Output {
        self.0.get(&index).unwrap()
    }
}

impl TryFrom<&[&str]> for CardSet {
    type Error = Error;

    fn try_from(value: &[&str]) -> Result<Self, Self::Error> {
        let parsed_card_results = value.iter().map(|line| line.parse::<Card>());

        if parsed_card_results.clone().any(|r| r.is_err()) {
            Err(anyhow!("Failed to parse CardSet"))
        } else {
            let hash_map: HashMap<usize, Card> = parsed_card_results
                .map(|r| r.unwrap())
                .map(|card| (card.id, card))
                .collect();

            Ok(Self(hash_map))
        }
    }
}

pub fn run(input: &[&str]) -> u32 {
    let card_set: CardSet = input.try_into().unwrap();

    let mut cards_to_process = Vec::from_iter(card_set.0.values());
    let mut cards_processed = 0u32;

    while !cards_to_process.is_empty() {
        cards_processed += cards_to_process.len() as u32;

        cards_to_process = cards_to_process
            .iter()
            .flat_map(|card| (1..=card.num_winners()).map(|i| &card_set[card.id + i as usize]))
            .collect();
    }

    cards_processed
}

#[cfg(test)]
mod tests {
    use super::*;

    use test_case::test_case;

    #[test]
    fn test_card_from_str() {
        let input = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53";

        let expected = Card {
            id: 1,
            winning_numbers: Box::new([41, 48, 83, 86, 17]),
            have_numbers: Box::new([83, 86, 6, 31, 17, 9, 48, 53]),
        };
        let actual: Card = input.parse().unwrap();

        assert!(actual == expected)
    }

    #[test_case(&"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53", 8)]
    #[test_case(&"Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19", 2)]
    #[test_case(&"Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1", 2)]
    #[test_case(&"Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83", 1)]
    #[test_case(&"Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36", 0)]
    #[test_case(&"Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11", 0)]
    fn test_calculate_score(input: &str, expected: u32) {
        let card: Card = input.parse().unwrap();

        assert!(card.calculate_score() == expected)
    }
}

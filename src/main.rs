#[macro_use]
extern crate lazy_static;

// mod day1;
// mod day2;
// mod day4;
// mod day5;
// mod day6;
// mod day7;
// mod day09;
// mod day10;
// mod day13;
mod day15;

fn main() -> std::io::Result<()> {
    let input = include_str!("../data/day15.txt");

    let result = day15::part2(input);

    println!("{}", result);
    Ok(())
}

use std::{cmp::Ordering, collections::HashMap, str::FromStr};

use anyhow::{anyhow, Error, Ok};

#[derive(Debug, PartialEq, Eq)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOAK,
    FullHouse,
    FourOAK,
    FiveOAK,
}

impl From<&HandType> for u64 {
    fn from(val: &HandType) -> Self {
        match val {
            HandType::HighCard => 0x000000,
            HandType::OnePair => 0x100000,
            HandType::TwoPair => 0x200000,
            HandType::ThreeOAK => 0x300000,
            HandType::FullHouse => 0x400000,
            HandType::FourOAK => 0x500000,
            HandType::FiveOAK => 0x600000,
        }
    }
}

impl PartialOrd for HandType {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self == other {
            Some(Ordering::Equal)
        } else if u64::from(self) > u64::from(other) {
            Some(Ordering::Greater)
        } else {
            Some(Ordering::Less)
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum Card {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

impl FromStr for Card {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "2" => Ok(Self::Two),
            "3" => Ok(Self::Three),
            "4" => Ok(Self::Four),
            "5" => Ok(Self::Five),
            "6" => Ok(Self::Six),
            "7" => Ok(Self::Seven),
            "8" => Ok(Self::Eight),
            "9" => Ok(Self::Nine),
            "T" => Ok(Self::Ten),
            "J" => Ok(Self::Jack),
            "Q" => Ok(Self::Queen),
            "K" => Ok(Self::King),
            "A" => Ok(Self::Ace),
            _ => Err(anyhow!("Not a valid card: {:?}", s)),
        }
    }
}

impl From<&Card> for u64 {
    fn from(val: &Card) -> Self {
        match val {
            Card::Two => 0x1,
            Card::Three => 0x2,
            Card::Four => 0x3,
            Card::Five => 0x4,
            Card::Six => 0x5,
            Card::Seven => 0x6,
            Card::Eight => 0x7,
            Card::Nine => 0x8,
            Card::Ten => 0x9,
            Card::Jack => 0xA,
            Card::Queen => 0xB,
            Card::King => 0xC,
            Card::Ace => 0xD,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Hand {
    hand_type: HandType,
    hand: Box<[Card]>,
}

impl Hand {
    pub fn new(hand: Box<[Card]>) -> Self {
        let hand_type = Self::calculate_type(&hand);

        Self { hand, hand_type }
    }

    fn calculate_type(hand: &[Card]) -> HandType {
        let mut hand_map: HashMap<&Card, u8> = HashMap::new();
        hand_map.reserve(5);

        hand.iter().for_each(|card| {
            if let Some(curr) = hand_map.get(card) {
                hand_map.insert(card, curr + 1);
            } else {
                hand_map.insert(card, 1);
            }
        });

        match hand_map.values().max() {
            Some(5) => {
                return HandType::FiveOAK;
            }
            Some(4) => {
                return HandType::FourOAK;
            }
            Some(3) if hand_map.len() == 2 => {
                return HandType::FullHouse;
            }
            Some(3) => {
                return HandType::ThreeOAK;
            }
            Some(_) => {}
            None => {
                panic!("Could not calculate maximum common card");
            }
        };

        match hand_map.len() {
            3 => HandType::TwoPair,
            4 => HandType::OnePair,
            _ => HandType::HighCard,
        }
    }

    fn score(&self) -> u64 {
        self.hand
            .iter()
            .rev()
            .enumerate()
            .fold(u64::from(&self.hand_type), |acc, (i, el)| {
                acc + (u64::from(el) * 0x10u64.pow(i as u32))
            })
    }
}

impl FromStr for Hand {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let hand: Box<[Card]> = s
            .chars()
            .map(|ch| ch.to_string().parse().unwrap())
            .collect();

        Ok(Self::new(hand))
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.score().partial_cmp(&other.score())
    }
}

pub fn run(input: &str) -> u64 {
    let mut sorted_hands = input
        .trim()
        .split('\n')
        .map(|line| {
            let mut iter = line.split_whitespace();

            (
                iter.next().unwrap().parse::<Hand>().unwrap(),
                iter.next().unwrap().parse::<u64>().unwrap(),
            )
        })
        .collect::<Box<_>>();

    sorted_hands.sort_by(|(a, _), (b, _)| a.partial_cmp(b).unwrap());

    sorted_hands
        .iter()
        .enumerate()
        .map(|(i, (_, bet))| bet * (i + 1) as u64)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    use test_case::test_case;
    use textwrap::dedent;

    #[test_case("AAAAA", HandType::FiveOAK)]
    #[test_case("AA8AA", HandType::FourOAK)]
    #[test_case("23332", HandType::FullHouse)]
    #[test_case("TTT98", HandType::ThreeOAK)]
    #[test_case("23432", HandType::TwoPair)]
    #[test_case("A23A4", HandType::OnePair)]
    #[test_case("23456", HandType::HighCard)]
    fn test_hand_type(input: &str, expected: HandType) {
        let actual: Hand = input.parse().unwrap();

        assert_eq!(actual.hand_type, expected)
    }

    #[test_case("AKQJT", 0x0DCBA9)]
    #[test_case("AAAAA", 0x6DDDDD)]
    #[test_case("23332", 0x412221)]
    fn test_score(input: &str, expected: u64) {
        let actual: Hand = input.parse().unwrap();

        assert_eq!(actual.score(), expected)
    }

    #[test_case("AAAAA", "AKQJT", Ordering::Greater)]
    #[test_case("AAAAA", "AAAAA", Ordering::Equal)]
    #[test_case("22333", "33444", Ordering::Less)]
    fn test_hand_cmp(input1: &str, input2: &str, expected: Ordering) {
        let hand1: Hand = input1.parse().unwrap();
        let hand2: Hand = input2.parse().unwrap();

        assert_eq!(hand1.partial_cmp(&hand2), Some(expected));
    }

    #[test]
    fn test_run() {
        let input = dedent(
            "
            32T3K 765
            T55J5 684
            KK677 28
            KTJJT 220
            QQQJA 483
        ",
        );

        assert_eq!(run(&input), 6440)
    }
}

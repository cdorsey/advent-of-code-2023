// https://adventofcode.com/2023/day/%DAY%
mod part1;

pub use self::part1::run as part1;

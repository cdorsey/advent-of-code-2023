#!/usr/bin/env just --justfile

RELEASE_BIN := "./target/release/" + `basename $(pwd)`

default: newday

D0 := `date +%d`
DN := `date +%e`
MOD := "src/day" + D0
# scaffold a new day
newday:
    @mkdir {{ MOD }}

    @cat _templates/dayN/mod.rs | sed -e "s/%DAY%/{{ DN }}/g" > {{ MOD }}/mod.rs
    @cp _templates/dayN/part1.rs {{ MOD }}/part1.rs

benchmark:
    @cargo build --release
    @hyperfine -Nw5 {{ RELEASE_BIN }}
